//
//  ViewController.swift
//  SafaBodaTest
//
//  Created by Johnstone Ananda on 22/02/2018.
//  Copyright © 2018 johnolwamba. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import RappleProgressHUD
import Alamofire

class ViewController: UIViewController {
    
    var originAirport: String = ""
    var destAirport: String = ""
    var originLongitude: Double = 0.0
    var originLatitude: Double = 0.0
    var destLongitude: Double = 0.0
    var destLatitude: Double = 0.0
    
     var latitudeArr: [Double] = []
     var longitudeArr: [Double] = []
     var codeArr: [String] = []
     var nameArr: [String] = []
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func viewDidAppear(_ animated: Bool) {
       
        if(Reachability.isConnectedToNetwork() == true){
                loadAirports()
        }else{
            DispatchQueue.main.async {
                RappleActivityIndicatorView.stopAnimation()
                
                _ = SweetAlert().showAlert("Whoops", subTitle: "No internet connection" as String, style: AlertStyle.error, buttonTitle:  "OK", buttonColor: UIColor(red:0.11, green:0.64, blue:0.73, alpha:1.0)) { (isOtherButton) -> Void in
                    if isOtherButton == true {
                        self.loadAirports()
                    }
                }
                
            }
        }
        
    }

    
    @IBAction func btnSelectOrigin(_ sender: UIButton) {
        ActionSheetStringPicker.show(withTitle: "Select Origin", rows: self.nameArr, initialSelection: 1, doneBlock: {
            picker, value, index in
            self.originAirport = "\(self.codeArr[value])"
            self.originLongitude = self.longitudeArr[value]
            self.originLatitude = self.latitudeArr[value]
            sender.setTitle("\(self.codeArr[value]) - \(index!)", for: .normal)
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
    
    @IBAction func btnSelectDestination(_ sender: UIButton) {
        ActionSheetStringPicker.show(withTitle: "Select Destination", rows: self.nameArr, initialSelection: 1, doneBlock: {
            picker, value, index in
            self.destAirport = "\(self.codeArr[value])"
            self.destLongitude = self.longitudeArr[value]
            self.destLatitude = self.latitudeArr[value]
            sender.setTitle("\(self.codeArr[value]) - \(index!)", for: .normal)
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
    
    
    @IBAction func btnSearch(_ sender: UIButton) {
        
        if(self.destAirport == "" || self.originAirport == ""){
          
            SweetAlert().showAlert("Sorry!", subTitle: "Please select the origin and destination airports", style: AlertStyle.warning)
            
        }else{
        
            let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "SchedulesViewController") as! SchedulesViewController
            secondViewController.originAirport = originAirport
            secondViewController.destAirport = destAirport
            secondViewController.destLongitude = destLongitude
            secondViewController.destLatitude = destLatitude
            secondViewController.originLongitude = originLongitude
            secondViewController.originLatitude = originLatitude
            
            self.navigationController?.pushViewController(secondViewController, animated: true)
            
        }
        
    }
    
    
    
    func loadAirports(){
        DispatchQueue.main.async {
            RappleActivityIndicatorView.startAnimatingWithLabel("loading...", attributes: RappleModernAttributes)
        }
        request("\(URL_BASE)airports/", method: .get, parameters: nil).responseJSON { response in
            DispatchQueue.main.async {
                RappleActivityIndicatorView.stopAnimation()
            }
            // make sure we got some JSON since that's what we expect
            guard let json = response.result.value as? [String: AnyObject] else {
                DispatchQueue.main.async {
                    RappleActivityIndicatorView.stopAnimation()
                    
                    _ = SweetAlert().showAlert("Whoops", subTitle: "Just a second. We're getting a few things...from space!" as String, style: AlertStyle.error, buttonTitle:  "OK", buttonColor: UIColor(red:0.11, green:0.64, blue:0.73, alpha:1.0)) { (isOtherButton) -> Void in
                        if isOtherButton == true {
                            self.loadAirports()
                        }
                    }
                    
                }
                return
            }
            
            guard let firstJsonCheck =  json["AirportResource"]!["Airports"] as? [String: AnyObject]  else{
                return
            }
            
            guard let airportData =  firstJsonCheck["Airport"] as? NSArray else{
                    return
            }
            
                if(airportData.count > 0){
                    self.clearArrays()
                    
                    for item in airportData{
                        if let airport_code = (item as AnyObject).value(forKey:"AirportCode") as? String {
                            self.codeArr.append(airport_code)
                        }
                        
                        if let locationObj = (item as AnyObject).value(forKey:"Position") as? NSObject {
                                if let cordinate = (locationObj as AnyObject).value(forKey:"Coordinate") as? NSObject {
                                    
                                  if let latitude = (cordinate as AnyObject).value(forKey:"Latitude") as? Double {
                                     self.latitudeArr.append(latitude)
                                    }
                                    
                                    if let longitude = (cordinate as AnyObject).value(forKey:"Longitude") as? Double {
                                        self.longitudeArr.append(longitude)
                                    }
                                }
                        }
                        
                        if let nameObj = (item as AnyObject).value(forKey:"Names") as? NSObject {
                            if let singleName = (nameObj as AnyObject).value(forKey:"Name") as? NSObject {
                                
                                if let name = (singleName as AnyObject).value(forKey:"$") as? String {
                                    self.nameArr.append(name)
                                }
                               
                            }
                        }
 
                    }
                
                    
                }else{
                 
                }
        }
    }
    
    
    

    
    func clearArrays(){
        latitudeArr.removeAll()
        longitudeArr.removeAll()
        codeArr.removeAll()
        nameArr.removeAll()
    }
    
    
}

