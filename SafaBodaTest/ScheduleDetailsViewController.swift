//
//  ScheduleDetailsViewController.swift
//  SafaBodaTest
//
//  Created by Johnstone Ananda on 25/02/2018.
//  Copyright © 2018 johnolwamba. All rights reserved.
//

import UIKit
import Alamofire
import GoogleMaps

class ScheduleDetailsViewController: UIViewController {
    
    var destLongitude:Double?
    var destLatitude:Double?
    var originLongitude:Double?
    var originLatitude: Double?
    
    @IBOutlet weak var ui_map: GMSMapView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {

        let path = GMSMutablePath()
        path.add(CLLocationCoordinate2D(latitude: self.destLatitude!, longitude: self.destLongitude!))
        path.add(CLLocationCoordinate2D(latitude: self.originLatitude!, longitude: self.originLongitude!))
        
        let rectangle = GMSPolyline(path: path)
        rectangle.map = ui_map
        
        
    }
  

}
