//
//  SchedulesViewController.swift
//  SafaBodaTest
//
//  Created by Johnstone Ananda on 25/02/2018.
//  Copyright © 2018 johnolwamba. All rights reserved.
//

import UIKit
import RappleProgressHUD
import Alamofire

class SchedulesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var originAirport:String?
    var destAirport:String?
    var destLongitude:Double?
    var destLatitude:Double?
    var originLongitude:Double?
    var originLatitude: Double?
    
    
    var departTimeArr: [String] = []
    var departAirportArr: [String] = []
    var arrivalAirportArr: [String] = []
    var arrivalTimeArr: [String] = []
    var arivalTerminalArr: [Int] = []
    var flightNoArr: [Int] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tableView.separatorStyle = .none
        self.tableView.reloadData()
     
        
        if(Reachability.isConnectedToNetwork() == true){
            loadSchedules()
        }else{
            DispatchQueue.main.async {
                RappleActivityIndicatorView.stopAnimation()
                
                _ = SweetAlert().showAlert("Whoops", subTitle: "No internet connection" as String, style: AlertStyle.error, buttonTitle:  "OK", buttonColor: UIColor(red:0.11, green:0.64, blue:0.73, alpha:1.0)) { (isOtherButton) -> Void in
                    if isOtherButton == true {
                        self.loadSchedules()
                    }
                }
                
            }
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return departTimeArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let Cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SchedulesTableViewCell
        
        Cell.departtime?.text = "\(departAirportArr[indexPath.row]) at \(departTimeArr[indexPath.row])"
        Cell.arrivaltime?.text = "\(arrivalAirportArr[indexPath.row]) at \(arrivalTimeArr[indexPath.row])"
        Cell.arrivalterminal?.text = "1"
        Cell.flightno?.text = "\(flightNoArr[indexPath.row])"
        
        return Cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "ScheduleDetailsViewController") as! ScheduleDetailsViewController
        secondViewController.destLongitude = destLongitude
        secondViewController.destLatitude = destLatitude
        secondViewController.originLongitude = originLongitude
        secondViewController.originLatitude = originLatitude
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    
    
    func loadSchedules(){
        DispatchQueue.main.async {
            RappleActivityIndicatorView.startAnimatingWithLabel("loading...", attributes: RappleModernAttributes)
        }
        
        request("\(URL_BASE)flightschedules?origin=\(String(describing: originAirport!))&destination=\(String(describing: destAirport!))", method: .get, parameters: nil).responseJSON { response in
            DispatchQueue.main.async {
                RappleActivityIndicatorView.stopAnimation()
            }
            // make sure we got some JSON since that's what we expect
            guard let json = response.result.value as? [String: AnyObject] else {
                DispatchQueue.main.async {
                    RappleActivityIndicatorView.stopAnimation()
                    
                    _ = SweetAlert().showAlert("Whoops", subTitle: "Just a second. We're getting a few things...from space!" as String, style: AlertStyle.error, buttonTitle:  "OK", buttonColor: UIColor(red:0.11, green:0.64, blue:0.73, alpha:1.0)) { (isOtherButton) -> Void in
                        if isOtherButton == true {
                            self.loadSchedules()
                        }
                    }
                    
                }
                return
            }
            
            if (json["ProcessingErrors"] as? [String: AnyObject]) != nil{
                DispatchQueue.main.async {
                let emptyLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
                emptyLabel.text = "No Schedule Found"
                emptyLabel.textColor = UIColor.black
                emptyLabel.textAlignment = NSTextAlignment.center
                self.tableView.backgroundView = emptyLabel
                self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
                }
             }else{
                
                guard let scheduleData =  json["ScheduleResource"]!["Schedule"] as? NSArray  else{
                    return
                }
                
                print(scheduleData)
                var i:Int = 0
                if(scheduleData.count > 0){
                    self.clearArrays()
                    for item in scheduleData{
                        
                        if let flightObj = (item as AnyObject).value(forKey:"Flight") as? NSObject {
                            if let flightNoObj = (flightObj as AnyObject).value(forKey:"MarketingCarrier") as? AnyObject{
                                if let flight_no = (flightNoObj as AnyObject).value(forKey:"FlightNumber") as? NSArray {
                                    self.flightNoArr.append(flight_no[i] as! Int)
                                }
                            }
                        }
                        if let flightObj = (item as AnyObject).value(forKey:"Flight") as? NSObject {
                            if let departureObj = (flightObj as AnyObject).value(forKey:"Departure") as? AnyObject{
                                if let depart_airport = (departureObj as AnyObject).value(forKey:"AirportCode") as? NSArray {
                                    self.departAirportArr.append(depart_airport[i] as! String)
                                }
                                
                                if let scheduledTimeObj = (departureObj as AnyObject).value(forKey:"ScheduledTimeLocal") as? NSArray{
                                    if let depart_time = (scheduledTimeObj[i] as AnyObject).value(forKey:"DateTime") as? String {
                                        self.departTimeArr.append(depart_time)
                                    }
                                }
                            }
                        }
                        if let flightObj = (item as AnyObject).value(forKey:"Flight") as? NSObject {
                            if let arrivalObj = (flightObj as AnyObject).value(forKey:"Arrival") as? NSObject{
                                
                                if let arrival_airport = (arrivalObj as AnyObject).value(forKey:"AirportCode") as? NSArray {
                                    self.arrivalAirportArr.append(arrival_airport[arrival_airport.count - 1] as! String)
                                }
                                
                                if let scheduledTimeObj = (arrivalObj as AnyObject).value(forKey:"ScheduledTimeLocal") as? NSArray{
                                    if let arrive_time = (scheduledTimeObj[i] as AnyObject).value(forKey:"DateTime") as? String {
                                        self.arrivalTimeArr.append(arrive_time)
                                    }
                                }
                                
                                if let terminalObj = (arrivalObj as AnyObject).value(forKey:"Terminal") as? NSArray{
                                    if let terminal = (terminalObj[i] as AnyObject).value(forKey:"Name") as? Int {
                                        self.arivalTerminalArr.append(terminal)
                                    }
                                }
                            }
                        }
                        i + 1
                    }
                    DispatchQueue.main.async {
                        self.tableView!.reloadData()
                    }
                    
                }else{
                    
                }
                
            }
           
           
            
            
        }
    }
    
 
    func clearArrays(){
        departTimeArr.removeAll()
        arrivalTimeArr.removeAll()
        arivalTerminalArr.removeAll()
        flightNoArr.removeAll()
        departAirportArr.removeAll()
        arrivalAirportArr.removeAll()
    }
    

}
