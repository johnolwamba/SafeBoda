//
//  SplashScreenViewController.swift
//  SafaBodaTest
//
//  Created by Johnstone Ananda on 22/02/2018.
//  Copyright © 2018 johnolwamba. All rights reserved.
//

import UIKit

class SplashScreenViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        perform(#selector(SplashScreenViewController.showHome), with: nil, afterDelay: 3)
    }
    
    
    @objc func showHome(){
        performSegue(withIdentifier: "showHome", sender: self)
    }
    
   
}
