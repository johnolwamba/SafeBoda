//
//  SchedulesTableViewCell.swift
//  SafaBodaTest
//
//  Created by Johnstone Ananda on 25/02/2018.
//  Copyright © 2018 johnolwamba. All rights reserved.
//

import UIKit

class SchedulesTableViewCell: UITableViewCell {

    @IBOutlet weak var departtime: UILabel!
    @IBOutlet weak var arrivaltime: UILabel!
    @IBOutlet weak var arrivalterminal: UILabel!
    @IBOutlet weak var flightno: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
